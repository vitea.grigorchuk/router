import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import HomePage from './pages/HomePage'
import ProfilesPage from './pages/ProfilesPage'
import ProfilePage from './pages/ProfilePage';


const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
    {
        path: '/',
        element: <HomePage />
    },
    {
        path: '/profiles',
        element: <ProfilesPage />,
        children: [
            {
                path: '/profiles/:profileId',
                element: <ProfilePage />
            }
        ]
    },

])


root.render(



    <RouterProvider router={router} />



);