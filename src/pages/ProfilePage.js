import { useParams } from "react-router-dom"

const ProfilePage = () => {
    const params = useParams()
    return (
        <>
            <p>ProfilePage {params.profileId}</p>
        </>
    )
}

export default ProfilePage