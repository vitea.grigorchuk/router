import { Link, Outlet } from "react-router-dom"
const ProfilesPage = () => {
    const profiles = [1, 2, 3, 4, 5]
    return (
        <div className="d">
            <p>ProfilesPage</p>
            <div className="r">
                {
                    profiles.map((profile) => (
                        <Link
                            className="test"
                            key={profile}
                            to={`/profiles/${profile}`}
                        >
                            Profile {profile}
                        </Link>

                    )
                    )
                }
            </div>

            <Outlet />
        </div>
    )
}

export default ProfilesPage